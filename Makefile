# This Makefile will build the application.

# Object files to create for the executable
OBJS = obj/Resource.o obj/batchscript_starter.o

# Warnings to be raised by the C compiler
WARNS = -Wall

# Names of tools to use when building
CC = gcc -g
RC = windres
EXE = batchscript_starter.exe

# Compiler flags. Compile ANSI build only if CHARSET=ANSI.
ifeq (${CHARSET}, ANSI)
  CFLAGS = -O2 -std=c++17 -D _WIN32_IE=0x0500 -D WINVER=0x0500 ${WARNS} -Iinclude
else
  CFLAGS = -O2 -std=c++17 -D UNICODE -D _UNICODE -D _WIN32_IE=0x0500 -D WINVER=0x0500 ${WARNS} -Iinclude
endif

# Linker flags
LDFLAGS = -lcomctl32 -Wl,--subsystem,windows

.PHONY: all clean

# Build executable by default
all: executable

# strip symbols for release
release: LDFLAGS += -s
release: executable

debug: CFLAGS += -D OUTER
debug: exectuable

executable: bin/${EXE}

# Delete all build output
clean:
	if exist bin\${EXE}  del /q bin\${EXE}
	if exist obj\*  del /q obj\*

# Create build output directories if they don't exist
bin obj:
	cmd /c @IF NOT EXIST "$@" mkdir "$@"

# Compile object files for executable
obj/%.o: src/%.cpp | obj
	${CC} ${CFLAGS} -c "$<" -o "$@"

# Build the resources
obj/Resource.o: res/Resource.rc res/Application.manifest res/Application.ico include/Resource.h | obj
	${RC} -I./include -I./res -i "$<" -o "$@"

# Build the exectuable
bin/${EXE}: ${OBJS} | bin
	${CC} -o "$@" ${OBJS} ${LDFLAGS}

# C header dependencies
obj/batchscript_starter.o:  include/targetver.h include/Resource.h